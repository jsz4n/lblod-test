let pkgs = import <nixpkgs> {};

in pkgs.mkShell rec {
    buildInputs = with pkgs; [
        nodejs-16_x yarn
        chromium #for testing purposes
    ];
    shellHook = '' 
      export PATH=$PATH:~/.npm-packages/bin
    '';
}
