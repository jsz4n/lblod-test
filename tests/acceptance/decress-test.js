import { module, test } from 'qunit';
import { visit, currentURL, findAll } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';

module('Acceptance | decress', function (hooks) {
  setupApplicationTest(hooks);
  test('visiting root works', async function (assert) {
    await visit('/');
    assert.strictEqual(currentURL(), '/');
  });

  test('visiting /decrees works', async function (assert) {
    await visit('/decrees');
    assert.strictEqual(currentURL(), '/decrees');
  });

  test('visiting /decrees display the right heading', async function (assert) {
    await visit('/decrees');
    assert.dom('h2').hasText('These are the last 5 decrees');
  });

  test('/decrees output a table with 5 decrees (rows)', async function (assert) {
    await visit('/decrees');
    const numberRows = findAll('tr');
    assert.strictEqual(numberRows.length, 5);
  });
});
