import Model from '@ember-data/model';

export default class DecreeModel extends Model {
  // I wanted to use @attr but the following was the most simple (for now)
  constructor(title, date) {
    super();
    this.date = date;
    this.title = title;
  }
}
