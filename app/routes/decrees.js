import Route from '@ember/routing/route';
import DecreeModel from '../models/decree';

export default class DecreesRoute extends Route {
  async model() {
    const sparqlQuery =
      `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX eli: <http://data.europa.eu/eli/ontology#>
      PREFIX datavl: <https://data.vlaanderen.be/id/concept/AardWetgeving/>
      SELECT DISTINCT ?date ?title
      FROM  <https://data.vlaanderen.be/ns/wetgeving>
      WHERE {
      ?s eli:type_document datavl:Decreet;
      eli:is_realized_by ?o.
      ?o a eli:LegalExpression;
      eli:title ?title;
      eli:date_publication ?date.
      } ORDER BY DESC(?date)  LIMIT 5`;

    // the route below does not work because of CORS origin
    //const endpoint = `https://codex.vlaanderen.be/sparql?query=${escape(sparqlQuery)}`;

    // I used the following url in order to work around the CORS Error
    const endpoint = `https://codex.opendata.api.vlaanderen.be:8888/sparql?query=${escape(
      sparqlQuery
    )}`;
    const response = await fetch(endpoint, {
      headers: {
        Accept: 'application/sparql-results+json',
        /*"Access-Control-Allow-Origin": '*',
          "Origin":"https://codex.vlaanderen.be",
          "Referer":"https://codex.vlaanderen.be"*/
      },
    });
    const responseObject = await response.json();
    const decreeBindings = responseObject.results.bindings;

    //console.log(decreeBindings);
    return decreeBindings.map(
      (e) => new DecreeModel(e.title.value, e.date.value)
    );
  }
}
